//
//  hangman.swift
//  hangman
//
//  Created by Jan Gregor Emge-Triebel on 15.06.21.
//
//  This struct holds the game logic and game state
//


import Foundation

struct Hangman {
    
    private var session: HangmanSession
    
    init(withSession session:HangmanSession) {
        self.session = session
    }
    
    public var guessCount: Int {
        return self.session.guessedLetters.count
    }
    
    public var wrongGuessCount: Int {
        var wrongLetters:[String] = []

        for guessedCharacter in self.session.guessedLetters {
            if (self.session.secret.lowercased().contains(guessedCharacter.lowercased()) == false) {
                wrongLetters.append(guessedCharacter.lowercased())
            }
        }
        
        return wrongLetters.count;
    }
    
    public var solved: Bool {
        for wordCharacter in self.session.secret {
            if (self.session.guessedLetters.contains(wordCharacter.lowercased()) == false) {
                return false
            }
        }
        
        return true;
    }
    
    public var length: Int {
        return self.session.secret.count
    }
    
    public var printable: String {
        var printable = "";
        var first = true
        for wordCharacter in self.session.secret {
            if (self.session.guessedLetters.contains(wordCharacter.lowercased())) {
                if (first) {
                    printable = printable + wordCharacter.uppercased()
                } else {
                    printable = printable + wordCharacter.lowercased()
                }
            } else {
                printable = printable + "_"
            }
            printable = printable + " "
            first = false;
        }
        
        return printable;
    }
    
    mutating public func guess(withLetter letter:String) throws -> Result {
        if (letter.count != 1) {
            throw HangmanError.invalidLetter
        }
        
        if (self.session.guessedLetters.contains(letter)) {
            throw HangmanError.duplicateLetter
        }
        
        self.session.addGuessedLetter(letter.lowercased())
        
        if (self.solved == true) {
            return GuessResult(withState: HangmanState.won)
        }
        
        if (self.wrongGuessCount == self.session.wrongGuessLimit) {
            return GuessResult(withState: HangmanState.lost)
        }
        
        return GuessResult(withState: HangmanState.ongoing)
    }
}
