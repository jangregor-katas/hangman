//
//  hangmanSupport.swift
//  hangman
//
//  Created by Jan Gregor Emge-Triebel on 16.06.21.
//
//  Here we keep all hangman game states and helper structures
//


import Foundation

struct HangmanSession {
    public let secret: String
    
    public var guessedLetters: [String] = []
    
    public let wrongGuessLimit: Int
    
    init(secret: String, withWrongGuessLimit wrongGuessLimit:Int) {
        self.secret = secret
        self.wrongGuessLimit = wrongGuessLimit
    }
    
    mutating public func addGuessedLetter(_ letter: String) {
        self.guessedLetters.append(letter)
    }
}

protocol Result {
    var state: HangmanState {get}
}

struct GuessResult: Result {
    public let state: HangmanState
    
    init(withState state:HangmanState) {
        self.state = state
    }
}

enum HangmanState {
    case won
    case lost
    case ongoing
}

enum HangmanError: Error {
    case invalidLetter
    case duplicateLetter
}
