//
//  game.swift
//  hangman
//
//  Created by Jan Gregor Emge-Triebel on 15.06.21.
//
//  This class is the clue code between user i/o and game logic and game state
//

import Foundation

class Game {
    
    var hangman: Hangman

    var isDone: Bool {
        return hangman.solved
    }
    
    init() {
        let hangmanSession = HangmanSession(secret: WordProvider.getRandomWord(), withWrongGuessLimit: 3)
        self.hangman = Hangman(withSession: hangmanSession)

        print("Your word has \(hangman.length) letters. You have \(hangmanSession.wrongGuessLimit) wrong guesses.")
        print(hangman.printable)
    }

    public func takeTurn() {
        let result = takeGuess()
        print(hangman.printable)

        if (result.state == .lost) {
            print("You have used up all your guesses and lost.")
            exit(0)
        }

        if (result.state == .won) {
            print("You guessed the word and won.")
            exit(0)
        }
    }

    private func takeGuess() -> Result {
        do {
            let letter = askForLetter()
            let result = try hangman.guess(withLetter: letter)
            return result
        } catch HangmanError.invalidLetter {
            print("Your guess was invalid, please try again.")
            return takeGuess()
        } catch HangmanError.duplicateLetter {
            print("You can only guess each letter once, please try again.")
            return takeGuess()
        } catch {
            return takeGuess()
        }
    }

    private func askForLetter() -> String {
        print("Enter a letter and press enter :")
        
        guard let letter = readLine() else { return askForLetter() }
        
        return letter
    }
}
