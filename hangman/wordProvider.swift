//
//  wordProvider.swift
//  hangman
//
//  Created by Jan Gregor Emge-Triebel on 16.06.21.
//
//  Simple provider of words
//

import Foundation

struct WordProvider {
    
    static let wordList = [
        "Developer",
        "User",
        "Programmer",
        "Application",
        "Compiler",
        "Debugger",
        "Stacktrace",
        "Keyboard",
    ]
    
    static func getRandomWord() -> String {
        return wordList.randomElement()!
    }
}
