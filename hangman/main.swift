//
//  main.swift
//  hangman
//
//  Created by Jan Gregor Emge-Triebel on 15.06.21.
//
//  This class is merely the entrypoint
//


import Foundation

let game = Game()

repeat {
    game.takeTurn()
} while (game.isDone == false)
